# Changelog
All notable changes to this project will be documented in this file.
Note: Changelogs are for HUMANS, not computers.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2022-07-27
### Added
- Reverted back to version 1 of the PSR logger for compatibility

## [1.0] - 2022-07-27
### Added
- Upgrade to PHP 8.1

## [0.4] - 2021-03-22
### Added
- PHP 8 support

## [0.3.3] - 2021-02-24
### Added
- This changelog.
- Docker for Composer / PHPUnit.
- Strict types.

### Changed
- Fixed deprecation in test for QueueProcessor.

### Removed
- Visibility timeout argument that overrides the configuration of the SQS queue.