<?php
declare(strict_types=1);

namespace BNNVARA\SimpleBusAwsBridge\Publisher;

use Aws\Sns\SnsClient;
use SimpleBus\Asynchronous\Publisher\Publisher;
use SimpleBus\Serialization\Envelope\Serializer\StandardMessageInEnvelopeSerializer;

class SnsPublisher implements Publisher
{
    public function __construct(
        private SnsClient $client,
        private StandardMessageInEnvelopeSerializer $serializer,
        private string $topic
    ){
    }

    public function publish(object $message): void
    {
        $this->client->publish(
            [
                'TopicArn' => $this->topic,
                'Message' => $this->serializer->wrapAndSerialize($message)
            ]
        );
    }
}