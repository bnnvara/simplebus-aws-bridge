<?php
declare(strict_types=1);

namespace BNNVARA\SimpleBusAwsBridge\Queue\Consumer;

use Exception;

class EmptyQueueException extends Exception
{

}