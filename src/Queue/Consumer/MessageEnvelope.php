<?php
declare(strict_types=1);

namespace BNNVARA\SimpleBusAwsBridge\Queue\Consumer;

class MessageEnvelope
{

    public function __construct(private $id, private $message)
    {
    }

    public function getId()
    {
        return $this->id;
    }

    public function getMessage()
    {
        return $this->message;
    }
}