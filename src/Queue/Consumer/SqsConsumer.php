<?php
declare(strict_types=1);

namespace BNNVARA\SimpleBusAwsBridge\Queue\Consumer;

use BNNVARA\SimpleBusAwsBridge\Queue\QueueName;
use Aws\Sqs\SqsClient;
use SimpleBus\SymfonyBridge\Bus\EventBus;

class SqsConsumer implements QueueConsumerInterface
{

    public function __construct(
        private SqsClient $client,
        private string $endPoint,
        private QueueToEnvelopeConverterInterface $messageToEnvelopeConverter,
        private EventBus $eventBus
    ){
    }

    /** @inheritdoc */
    public function consume(QueueName $queue): void
    {
        $message = $this->getMessageFromQueue($queue);
        $envelope = $this->messageToEnvelopeConverter->convert($message);
        $this->eventBus->handle($envelope->getMessage());
        $this->acknowledgeMessage($queue, $envelope);
    }

    /**
     * @throws EmptyQueueException
     */
    private function getMessageFromQueue(QueueName $queue): array
    {
        $messages = $this->client->receiveMessage(
            [
                'QueueUrl' => sprintf('%s%s', $this->endPoint, $queue)
            ]
        )->get('Messages');

        if ($messages === null) {
            throw new EmptyQueueException();
        }
        /** @var array $messages */
        /** @var array $message */
        $message = reset($messages);
        return $message;
    }

    private function acknowledgeMessage(QueueName $queue, MessageEnvelope $envelope): void
    {
        $this->client->deleteMessage([
            'QueueUrl' => sprintf('%s%s', $this->endPoint, $queue),
            'ReceiptHandle' => $envelope->getId()
        ]);
    }
}
