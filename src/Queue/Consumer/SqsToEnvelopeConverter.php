<?php
declare(strict_types=1);

namespace BNNVARA\SimpleBusAwsBridge\Queue\Consumer;

use JMS\Serializer\SerializerInterface;

class SqsToEnvelopeConverter implements QueueToEnvelopeConverterInterface
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    public function convert(array $message): MessageEnvelope
    {
        $messageObject = json_decode($message['Body']);
        $eventData = json_decode($messageObject->Message);
        $event = $this->serializer->deserialize($eventData->serialized_message, $eventData->message_type, 'json');

        return new MessageEnvelope($message['ReceiptHandle'], $event);
    }
}
