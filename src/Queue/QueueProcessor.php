<?php
declare(strict_types=1);

namespace BNNVARA\SimpleBusAwsBridge\Queue;

use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\EmptyQueueException;
use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\QueueConsumerInterface;

class QueueProcessor
{

    public function __construct(private QueueConsumerInterface $queueConsumer)
    {
    }

    public function consume(QueueName $queueName): void
    {
        while($this->forever())
        {
            try {
                $this->queueConsumer->consume($queueName);
            } catch(EmptyQueueException $e)
            {
                sleep(1);
            }
        }
    }

    protected function forever(): bool
    {
        return true;
    }
}