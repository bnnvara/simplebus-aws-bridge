<?php
declare(strict_types=1);

namespace BNNVARA\SimpleBusAwsBridge\Queue;

class QueueName
{
    public function __construct(private string $name)
    {
    }

    public function __toString(): string
    {
        return $this->name;
    }
}