<?php
declare(strict_types=1);

namespace Tests\BNNVARA\SimpleBusAwsBridge\Queue;

use BNNVARA\SimpleBusAwsBridge\Queue\QueueName;
use PHPUnit\Framework\TestCase;

class QueueNameTest extends TestCase
{
    /** @test */
    public function aQueueNameCanBeGivenAndQueueCanBeCastedToString()
    {
        $queue = new QueueName('just-a-queue-name');

        $this->assertEquals('just-a-queue-name', (string) $queue);
    }
}