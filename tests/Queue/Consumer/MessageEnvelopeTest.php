<?php
declare(strict_types=1);

namespace Tests\BNNVARA\SimpleBusAwsBridge\Queue\Consumer;

use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\MessageEnvelope;
use PHPUnit\Framework\TestCase;

class MessageEnvelopeTest extends TestCase
{
    /** @test */
    public function anEnvelopeAlwaysContainsAnEventAndAnId()
    {
        $envelope = new MessageEnvelope('id-1', 'message');

        $this->assertEquals('id-1', $envelope->getId());
        $this->assertEquals('message', $envelope->getMessage());
    }
}
