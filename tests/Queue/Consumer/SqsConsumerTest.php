<?php
declare(strict_types=1);

namespace Tests\BNNVARA\SimpleBusAwsBridge\Queue\Consumer;

use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\EmptyQueueException;
use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\QueueToEnvelopeConverterInterface;
use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\SqsConsumer;
use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\MessageEnvelope;
use BNNVARA\SimpleBusAwsBridge\Queue\QueueName;
use Aws\Result;
use Aws\Sqs\SqsClient;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use SimpleBus\SymfonyBridge\Bus\EventBus;

class SqsConsumerTest extends TestCase
{
    /** @test */
    public function messagesAreRetrievedFromTheQueue()
    {
        /** @var MockObject $client */
        $client = $this->createSqsClient();

        $client->method('receiveMessage')
            ->with([
                'QueueUrl' => 'full-path-excluding-name-to-queuejust-a-queue-name'
            ])
            ->will($this->returnValue(new Result(['Messages' => [[]]])));

        $client->expects($this->once())
            ->method('receiveMessage');

        /** @var SqsClient $client */
        $consumer = $this->createSqsConsumer($client, $this->createMessageToEnvelopeConverterWithConvertCall());

        $consumer->consume(new QueueName('just-a-queue-name'));
    }

    /** @test */
    public function messagesAreDeletedFromTheQueue()
    {
        /** @var MockObject|SqsClient $client */
        $client = $this->createSqsClient();

        $client->method('receiveMessage')
            ->will($this->returnValue(new Result(['Messages' => [[]]])));

        $client->expects($this->once())
            ->method('deleteMessage');

        $consumer = $this->createSqsConsumer($client, $this->createMessageToEnvelopeConverterWithConvertCall());

        $consumer->consume(new QueueName('just-a-queue-name'));
    }

    /** @test */
    public function anExceptionIsThrownIfNoMessageFoundOnQueue()
    {
        $this->expectException(EmptyQueueException::class);

        /** @var MockObject $client */
        $client = $this->createSqsClient();

        $client->expects($this->once())
            ->method('receiveMessage')
            ->will($this->returnValue(new Result([])));

        /** @var SqsClient $client */
        $consumer = $this->createSqsConsumer($client, $this->createSqsToEnvelopeConverterInterface());
        $consumer->consume(new QueueName('just-a-queue-name'));
    }

    private function createSqsConsumer(SqsClient $client, QueueToEnvelopeConverterInterface $envelopeConverter): SqsConsumer
    {
        return new SqsConsumer(
            $client,
            'full-path-excluding-name-to-queue',
            $envelopeConverter,
            new EventBus()
        );
    }

    private function createMessageToEnvelopeConverterWithConvertCall(): QueueToEnvelopeConverterInterface
    {
        /** @var MockObject $converter */
        $converter = $this->createSqsToEnvelopeConverterInterface();
        $converter
            ->expects($this->once())
            ->method('convert')
            ->willReturn($this->getMessageEnvelopeMock());

        /** @var QueueToEnvelopeConverterInterface $converter */
        return $converter;
    }

    private function createSqsToEnvelopeConverterInterface(): QueueToEnvelopeConverterInterface
    {
        $converter = $this->getMockBuilder(QueueToEnvelopeConverterInterface::class)->getMock();

        /** @var QueueToEnvelopeConverterInterface $converter */
        return $converter;
    }

    private function createSqsClient(): SqsClient
    {
        /** @var MockObject $client */
        $client = $this->getMockBuilder(SqsClient::class)
            ->disableOriginalConstructor()
            ->addMethods(['receiveMessage', 'deleteMessage'])
            ->getMock();

        /** @var SqsClient $client */
        return $client;
    }

    private function getMessageEnvelopeMock(): MessageEnvelope
    {
        $messageEnvlope = $this->getMockBuilder(MessageEnvelope::class)->disableOriginalConstructor()->getMock();

        $messageEnvlope->expects($this->once())->method('getMessage')->willReturn(new class{});

        /** @var MessageEnvelope $messageEnvlope */
        return $messageEnvlope;
    }
}
