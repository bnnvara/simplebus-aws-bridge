<?php
declare(strict_types=1);

namespace Tests\BNNVARA\SimpleBusAwsBridge\Queue\Consumer;

class DummyEvent
{
    private $id;

    private $name;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }
}
