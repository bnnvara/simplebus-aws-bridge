<?php
declare(strict_types=1);

namespace Tests\BNNVARA\SimpleBusAwsBridge\Queue\Consumer;

use Aws\Result;
use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\MessageEnvelope;
use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\SqsToEnvelopeConverter;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class SqsToEnvelopeConverterTest extends TestCase
{
    /** @test */
    public function converterReturnsEnvelope(): void
    {
        $converter = $this->getConverter($this->getSerializerWithDeserializeCall());

        $envelope = $converter->convert(
            [
                'ReceiptHandle' => 'receipt-handle',
                'Body' => str_replace(' ', '', file_get_contents(__DIR__ . '/stub/message-stub.json')),
            ]
        );

        $this->assertInstanceOf(MessageEnvelope::class, $envelope);
        $this->assertEquals('receipt-handle', $envelope->getId());
        $this->assertInstanceOf(DummyEvent::class, $envelope->getMessage());
    }

    private function getConverter(SerializerInterface $serializer): SqsToEnvelopeConverter
    {
        return new SqsToEnvelopeConverter($serializer);
    }

    private function getSerializerWithDeserializeCall(): SerializerInterface
    {
        /** @var MockObject $serializer */
        $serializer = $this->getSerializerInterface();

        $serializer->expects($this->once())
            ->method('deserialize')
            ->will($this->returnValue(new DummyEvent()));

        /** @var SerializerInterface $serializer */
        return $serializer;
    }

    private function getSerializerInterface(): SerializerInterface
    {
        $serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();

        /** @var SerializerInterface $serializer */
        return $serializer;
    }
}