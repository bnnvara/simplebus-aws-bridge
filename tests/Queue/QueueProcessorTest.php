<?php
declare(strict_types=1);

namespace Tests\BNNVARA\SimpleBusAwsBridge\Queue;

use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\EmptyQueueException;
use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\QueueConsumerInterface;
use BNNVARA\SimpleBusAwsBridge\Queue\QueueName;
use BNNVARA\SimpleBusAwsBridge\Queue\QueueProcessor;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class QueueProcessorTest extends TestCase
{
    /** @test */
    public function itRunsForeverTheGivenQueue()
    {
        $queueProcessor = $this->getQueueProcessor($this->getQueueConsumerWithXConsumeCalls(2));

        $queueProcessor->consume(new QueueName('just-a-queue-name'));
    }

    /** @test */
    public function consumerIsPausedForOneSecondIfQueueIsEmpty()
    {
        $queueProcessor = $this->getQueueProcessor($this->getExceptionThrowingQueueConsumer());

        $start = time();
        $queueProcessor->consume(new QueueName('empty-queue'));
        $this->assertGreaterThanOrEqual(2, time() - $start);
    }

    private function getQueueProcessor(QueueConsumerInterface $queueConsumer): QueueProcessor
    {
        $mock = $this->getMockBuilder(QueueProcessor::class)
            ->setConstructorArgs([$queueConsumer])
            ->onlyMethods(['forever'])
            ->getMock();

        $mock
            ->expects($this->exactly(3))
            ->method('forever')
             ->willReturnOnConsecutiveCalls(
                 $this->returnValue(true),
                 $this->returnValue(true),
                 $this->returnValue(false)
             );

        /** @var QueueProcessor $mock */
        return $mock;
    }

    private function getQueueConsumerWithXConsumeCalls(int $numberOfCalls): QueueConsumerInterface
    {
        /** @var MockObject $consumer */
        $consumer = $this->getQueueConsumerInterface();

        $consumer->expects($this->exactly($numberOfCalls))->method('consume');

        /** @var QueueConsumerInterface $consumer */
        return $consumer;
    }

    private function getQueueConsumerInterface(): QueueConsumerInterface
    {
        $queueConsumer = $this->getMockBuilder(QueueConsumerInterface::class)->getMock();

        /** @var QueueConsumerInterface $queueConsumer */
        return $queueConsumer;
    }

    private function getExceptionThrowingQueueConsumer(): QueueConsumerInterface
    {
        /** @var MockObject $queueConsumer */
        $queueConsumer = $this->getQueueConsumerInterface();

        $queueConsumer->expects($this->any())
            ->method('consume')
            ->will($this->throwException(new EmptyQueueException()));

        /** @var QueueConsumerInterface $queueConsumer */
        return $queueConsumer;
    }
}