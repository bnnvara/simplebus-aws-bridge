<?php
declare(strict_types=1);

namespace Tests\BNNVARA\SimpleBusAwsBridge\Publisher;

use Aws\Sns\SnsClient;
use BNNVARA\SimpleBusAwsBridge\Publisher\SnsPublisher;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use SimpleBus\Serialization\Envelope\Serializer\StandardMessageInEnvelopeSerializer;

class SnsPublisherTest extends TestCase
{
    /** @test */
    public function anMessageIsPublishedToTheQueue()
    {
        $publisher = $this->getPublisher($this->getSnsClientWithPublishCall());

        $publisher->publish(new class{});
    }

    private function getPublisher(SnsClient $snsClient): SnsPublisher
    {
        return new SnsPublisher($snsClient, $this->getSerializer(), 'topic-name');
    }

    private function getSerializer(): StandardMessageInEnvelopeSerializer
    {
        $mock = $this->getMockBuilder(StandardMessageInEnvelopeSerializer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mock->expects($this->once())->method('wrapAndSerialize')->will($this->returnValue('message'));

        /** @var StandardMessageInEnvelopeSerializer $mock */
        return $mock;
    }

    private function getSnsClientWithPublishCall(): SnsClient
    {
        /** @var MockObject $client */
        $client = $this->getSnsClient();

        $client->expects($this->once())
            ->method('__call')
            ->with(
                $this->identicalTo('publish'),
                [
                    [
                        'TopicArn' => 'topic-name',
                        'Message' => 'message'
                    ]
                ]
            );

        return $client;
    }

    private function getSnsClient(): SnsClient
    {
        $snsClient = $this->getMockBuilder(SnsClient::class)->disableOriginalConstructor()->getMock();

        /** @var SnsClient $snsClient */
        return $snsClient;
    }
}