#!/bin/sh

apt-get update && apt-get install -y git unzip libzip-dev zip libicu-dev
docker-php-ext-install bcmath zip intl
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
composer install
export ENV=dev

vendor/bin/phpunit -c phpunit.xml.dist
